﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPClass.Bicycles.Behaviours
{
    internal interface IDrivable
    {
        void Accelerate();
        void Decelerate();
        void Steer(int direction);
    }
}
