﻿using OOPClass.Bicycles.Behaviours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPClass.Bicycles
{
    internal class BmxBike : Bicycle, IDrivable
    {
        public BmxBike(string brand, string description, 
            double price, int pegs) : base(brand, description, price)
        {
            Pegs = pegs;
        }

        public int Pegs { get; set; }

        public void Accelerate()
        {
            Console.WriteLine("I can feel the monster running through my veins");
        }

        public void Decelerate()
        {
            Console.WriteLine("Life is tough in the fast lane");
        }

        public void DoTrick()
        {
            Console.WriteLine("I am far too cool for you");
        }

        public override void MakeNoise()
        {
            Console.WriteLine("YO yo");
        }

        public override void PrintStats()
        {
            base.PrintStats();
            Console.WriteLine(Pegs);
        }

        public void Steer(int direction)
        {
           Console.WriteLine("Watch out for that tree, go:" + direction);
        }
    }
}
