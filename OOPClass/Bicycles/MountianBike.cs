﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OOPClass.Bicycles.Utility;

namespace OOPClass.Bicycles
{
    internal class MountianBike : Bicycle
    {
        public RacingCategories Category { get; set; }
        public MountianBike(string brand, string description, double price, RacingCategories category) : base(brand, description, price)
        {
            Category = category;
        }

        public override void MakeNoise()
        {
            Console.WriteLine("Nice hemlet");
        }
    }
}
