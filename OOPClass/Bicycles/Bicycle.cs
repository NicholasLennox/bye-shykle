﻿using OOPClass.Bicycles.Behaviours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPClass.Bicycles
{
    internal abstract class Bicycle
    {
        public string Brand { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public List<string> Keywords { get; set; }

        public Bicycle(string brand, string description, double price)
        {
            Brand = brand;
            Description = description;
            Price = price;
        }

        public virtual void PrintStats()
        {
            Console.WriteLine(Brand);
            Console.WriteLine(Description);
            Console.WriteLine(Price);
        }

        public abstract void MakeNoise();
    }
}
