﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPClass.Bicycles.Shops
{
    internal class BicycleShop
    {
        public List<Bicycle> Bicycles { get; set; }

        public BicycleShop(List<Bicycle> bicycles)
        {
            Bicycles = bicycles;
        }

        public void AddToStore(Bicycle bike)
        {
            Bicycles.Add(bike);
        }
    }
}
