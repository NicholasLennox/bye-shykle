﻿using OOPClass.Bicycles;
using OOPClass.Bicycles.Behaviours;
using OOPClass.Bicycles.Shops;
using OOPClass.Bicycles.Utility;

namespace OOPClass
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            BmxBike bmx = new BmxBike("1", "1", 1, 1);
            MountianBike mountain = new MountianBike("", "", 1, RacingCategories.WeakSauce);
            bmx.PrintStats();
            if (mountain.Category == RacingCategories.None)
                Console.WriteLine("Pleb");
            DoStuffToBike(bmx);
            DoStuffToBike(mountain);
            DoDriving(bmx);
            List<Bicycle> list = new List<Bicycle>
            {
                bmx,
                mountain
            };
            BicycleShop shop= new BicycleShop(list);

            Coords coords = new Coords();
            coords.y = 1;
            coords.x = 1;
            coords.PrintCoords();
        }

        public static void DoStuffToBike(Bicycle bike)
        {
            bike.PrintStats();
            Console.Write((BmxBike)bike);
        }

        public static void DoDriving(IDrivable driver)
        {
            driver.Accelerate();
            driver.Decelerate();
            driver.Steer(90);
        }
    }
}